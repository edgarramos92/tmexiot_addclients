from flask import Flask, render_template, url_for, flash, redirect, session, request
from forms import RegistrationForm, LoginForm
import mysql.connector
from datetime import datetime

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
conn = mysql.connector.connect(host="localhost",user="edgarramos",passwd="Ganjahman92", database = "tmexiot_db")
cursor = conn.cursor()

posts = [   {'author': 'Corey Schafer','title': 'Blog Post 1','content': 'First post content','date_posted': 'April 20, 2018'},{'author': 'Jane Doe','title': 'Blog Post 2','content': 'Second post content','date_posted': 'April 21, 2018' }]



@app.route("/")
@app.route("/index")
def index():
	return render_template('index.html', posts=posts)

@app.route("/showprofile")
def showprofile():
	
	
	def dicts_perfil():
		perfil_dict = [{'user' : session.get('user'), 'date' : datetime.now(), 'id': session.get('id')}]
		return perfil_dict
	def dicts_serv():
		cursor.execute("SELECT * FROM tbl_tmexiot_user WHERE tmexiot_user_id = %s", (session.get('id'),))
		res = cursor.fetchall()
		print(res)
		
		dict_serv = []
		if res[0][9] == 1:
			a = {'user': session.get('user'), 'date' : datetime.now(), 'id': session.get('id')}
			a['serv_name'] = 'Estacionamiento'
			a['serv1'] = 1
			
			dict_serv.append(a)
		else:
			a = {'user': session.get('user'), 'date' : datetime.now(), 'id': session.get('id')}
			a['serv_name'] = 'Estacionamiento'
			a['serv1'] = 0
			dict_serv.append(a)
		if res[0][10] == 1:
			a = {'user': session.get('user'), 'date' : datetime.now(), 'id': session.get('id')}
			a['serv2'] = 1
			dict_serv.append(a)
		else:
			a = {'user': session.get('user'), 'date' : datetime.now(), 'id': session.get('id')}
			a['serv2'] = 0
			dict_serv.append(a)
				
		
		return dict_serv
	if session.get('id'):
		#val = (session.get('id'))
		#cursor.execute("SELECT reg_serv1 FROM tbl_user WHERE (tmexiot_user_id) = (%s)", val)
		#print(session)
		_dict_perfil = dicts_perfil()
		_dict_serv = dicts_serv()
		return render_template('profile.html', user=session.get('user'))
		#return render_template('home.html', posts=posts)
	else:
		return render_template('errorLog.html',error = 'Acceso no autorizado')

@app.route("/home")
def home():
	if session.get('id'):
		#val = (session.get('id'))
		#cursor.execute("SELECT reg_serv1 FROM tbl_user WHERE (tmexiot_user_id) = (%s)", val)
		#print(session)
		return render_template('home.html', user=session.get('user'))
		#return render_template('home.html', posts=posts)
	else:
		return render_template('errorLog.html',error = 'Acceso no autorizado')

@app.route('/addclient')
def addclient():
	try:
		if session.get('id'):
			pag = "Deposito", "Cargo a Tarjeta", "Efectivo"
			serv = "Tracking", "Estacionamiento", "Serv3"
			
			return render_template('adduser.html',servs=serv, pago = pag)
		else:
			return render_template('errorLog.html', error="Acceso no autorizado")
	except Exception as e:
		#flash('Login Unsuccessful. Please check username and password', 'danger')
		return render_template('errorLog.html', error=e)		


@app.route('/addclients', methods=['POST'])
def addclients():
	#flash('You have been logged in!', 'success')
	try:
		if session.get('id'): 
			_name = request.form['inputname']
			_email= request.form['inputemail']
			_pass = request.form['inputpassword']
			_passconf = request.form['inputconfirm']
			_addres = request.form['inputaddres']
			_state = request.form['inputstate']
			_city = request.form['inputcity']
			_rfc = request.form['inputrfc']
			_pago = request.form['inputpago']
			_serv1 = request.form['inputserv1']
			_serv2 = request.form['inputserv2']
			_serv3 = request.form['inputserv3']
			print(_name,_email,_pass,_passconf,_addres,_state, _city, _rfc, _pago, _serv1, _serv2, _serv3)

			#sql = "SELECT * FROM tbl_tmexiot_user WHERE (reg_user, reg_pass) = (%s,%s)"
			#val = (_email,_password)
			cursor.execute(sql, val)
			res = cursor.fetchall()
			#print (res,len(res))
			a = "id","user","serv1","serv2"
			if len(res) > 0:
			
				session['id']= res[0][0]
				session['user']= res[0][1]
				session['serv1']= res[0][9]
				session['serv2']= res[0][10]
				message = "Iniciaste Sesión como: {}".format(res[0][1])
				flash(message, 'success')
				return redirect('/home')

			else:
				return render_template('errorLog.html',error = 'Usuario o contraseña incorrecta')


	except Exception as e:
		#flash('Login Unsuccessful. Please check username and password', 'danger')
		return render_template('errorLog.html', error=e)



@app.route('/showlogin')
def showlogin():
	return render_template('login.html')

@app.route('/logout')
def logout():
	session.pop('id',None) 
	return redirect('/')
    

@app.route("/about")
def about():
	#if session.get('id'):
		return render_template('about.html', title='About')
	#else:
	#	return render_template('errorLog.html',error = 'Acceso no autorizado')
	
#@app.route('/showprofile')
#def showprofile():
#	return render_template('profile.html')
	
@app.route('/showservices')
def showservices():
	return render_template('services.html')

@app.route("/register", methods=['GET', 'POST'])
def register():
	form = RegistrationForm()
	if form.validate_on_submit():
		#flash('Account created for {form.username.data}!', 'success')
		return redirect(url_for('home'))
	#return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
	#flash('You have been logged in!', 'success')
	try:

		_email = request.form['inputemail']
		_password = request.form['inputpassword']
		
		sql = "SELECT * FROM tbl_tmexiot_users WHERE (reg_email, reg_pass) = (%s,%s)"
		val = (_email,_password)
		cursor.execute(sql, val)
		res = cursor.fetchall()
		#print (res,len(res))
		a = "id","user","serv1","serv2"
		if len(res) > 0:
		
			session['id']= res[0][0]
			session['user']= res[0][1]
			
			message = "Iniciaste Sesión como: {}".format(res[0][1])
			flash(message, 'success')
			return redirect('/home')

		else:
		   	return render_template('errorLog.html',error = 'Usuario o contraseña incorrecta')


	except Exception as e:
		#flash('Login Unsuccessful. Please check username and password', 'danger')
		return render_template('errorLog.html', error=e)


if __name__ == '__main__':
	app.run(debug=True, port = 5000)
